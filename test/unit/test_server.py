import time
import unittest
from unittest.mock import patch

import zmq

from album.socket.server import AlbumSocketServer


# @patch("album.core.controller.collection.catalog_handler.CatalogHandler._retrieve_catalog_meta_information",
#        return_value={"name": "catalog_local", "version": "0.1.0"})
class TestServer(unittest.TestCase):
    pass
    # port = 7896
    # server = AlbumSocketServer(port)
    #
    # def setUp(self) -> None:
    #     TestCommon.setUp(self)
    #     self.setup_album_instance()
    #     self.setup_collection()
    #     self.server.setup(self.album)
    #     self.server.start()
    #     self.context = zmq.Context()
    #
    # def tearDown(self):
    #     TestCommon.tearDown(self)
    #     time.sleep(1)
    #     self.server.shutdown()
    #     self.context.term()
    #
    # def test(self, _):
    #     rep = self.context.socket(zmq.REQ)
    #     rep.connect("tcp://127.0.0.1:%s" % self.server.port)
    #     time.sleep(1)
    #     try:
    #         rep.send_pyobj({"action": "index"})
    #         message = rep.recv_pyobj()
    #         print(f"Received reply: {message}")
    #     finally:
    #         rep.disconnect("tcp://127.0.0.1:%s" % self.server.port)
    #         rep.close()

    # def test_root(self, client, _):
    #     json = self.getJSONResponse(client, "/")
    #     self.assertIsNotNone(json["version"])
    #     self.assertIsNotNone(json["author"])
    #     self.assertIsNotNone(json["email"])
    #
    # def test_index(self, client, _):
    #     self.setup_collection()
    #     json = self.getJSONResponse(client, "/index")
    #     self.assertIsNotNone(json["catalogs"])
    #
    # @patch("album.core.controller.run_manager.RunManager.run", return_value=None)
    # def test_run(self, client, route, _):
    #     self.__test_solution_route(client, "run", route)
    #
    # @patch("album.core.controller.install_manager.InstallManager.install", return_value=None)
    # def test_install(self, client, route, _):
    #     self.__test_solution_route(client, "install", route)
    #
    # @patch("album.core.controller.install_manager.InstallManager.uninstall", return_value=None)
    # def test_uninstall(self, client, route, _):
    #     self.__test_solution_route(client, "uninstall", route)
    #
    # @patch("album.core.controller.test_manager.TestManager.test", return_value=None)
    # def test_test(self, client, route, _):
    #     self.__test_solution_route(client, "test", route)
    #
    # @patch("album.core.controller.collection.collection_manager.CatalogHandler.add_by_src", return_value=Catalog(1, "", ""))
    # def test_add_catalog(self, client, route, _):
    #     json = self.getJSONResponse(client, "/add-catalog?src=CATALOG_URL")
    #     self.assertIsNotNone(json)
    #     self.assertEqual(1, json["catalog_id"])
    #     self.assertEqual(1, route.call_count)
    #
    # @patch("album.core.controller.collection.collection_manager.CatalogHandler.remove_from_collection_by_src", return_value=None)
    # def test_remove_catalog(self, client, route, _):
    #     json = self.getJSONResponse(client, "/remove-catalog?src=CATALOG_URL")
    #     self.assertIsNotNone(json)
    #     self.assertEqual(1, route.call_count)
    #
    # @patch("album.core.controller.clone_manager.CloneManager.clone", return_value=None)
    # def test_clone_catalog(self, client, route, _):
    #     json = self.getJSONResponse(client, f"/clone/catalog?target_dir={self.tmp_dir.name}&name=my-name")
    #     self.assertIsNotNone(json)
    #     self.server.task_manager().finish_tasks()
    #     route.assert_called_once_with("catalog", self.tmp_dir.name, "my-name")
    #
    # @patch("album.core.controller.clone_manager.CloneManager.clone", return_value=None)
    # def test_clone_solution(self, client, route, _):
    #     json = self.getJSONResponse(client, f"/clone/group/name/version?target_dir={self.tmp_dir.name}&name=my-name")
    #     self.assertIsNotNone(json)
    #     self.server.task_manager().finish_tasks()
    #     route.assert_called_once_with("group:name:version", self.tmp_dir.name, "my-name")
    #
    # @patch("album.core.controller.clone_manager.CloneManager.clone", return_value=None)
    # def test_clone_solution_by_path(self, client, route, _):
    #     json = self.getJSONResponse(client, f"/clone?path=my-path&target_dir={self.tmp_dir.name}&name=my-name")
    #     self.assertIsNotNone(json)
    #     self.server.task_manager().finish_tasks()
    #     route.assert_called_once_with("my-path", self.tmp_dir.name, "my-name")
    #
    # @patch("album.core.controller.search_manager.SearchManager.search", return_value={})
    # def test_search(self, client, route, _):
    #     json = self.getJSONResponse(client, "/search/searchterm")
    #     self.assertIsNotNone(json)
    #     self.assertEqual(1, route.call_count)
    #
    # def __test_solution_route(self, client, route, route_mock):
    #     resolve_mock = MagicMock(return_value = None)
    #     self.server.album_instance.resolve = resolve_mock
    #     json = self.getJSONResponse(client, "/%s/catalog_local/group/name/version" % route)
    #     self.assertIsNotNone(json)
    #     self.server.task_manager().finish_tasks()
    #     self.assertEqual(1, route_mock.call_count)
    #     json = self.getJSONResponse(client, "/%s/group/name/version" % route)
    #     self.assertIsNotNone(json)
    #     self.server.task_manager().finish_tasks()
    #     self.assertEqual(2, route_mock.call_count)


if __name__ == '__main__':
    unittest.main()
